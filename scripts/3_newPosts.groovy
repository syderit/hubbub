import com.grailsinaction.User
import com.grailsinaction.Post

def user = new User(loginId: 'juan18', password: 'secret')
if (user.validate()) {
    user.save()
    println "User id=$user.id"
    
    def post1 = new Post(content: 'el 1er post de juan')
    def post2 = new Post(content: 'el 2do post de juan')
    def post3 = new Post(content: 'el 3er post de juan')
    
    user.addToPosts(post1)
    user.addToPosts(post2)
    user.addToPosts(post3)
    
    def dbUser = User.get(user.id)
    
    def sortedPostContent = dbUser.posts.collect {
        it.content
    }.sort()
    
    sortedPostContent.each{
        println "encontrado post: $it"
    }
} else {
    prinln "user data not valid"
}