import com.grailsinaction.Post
import grails.util.Environment

Environment.executeForCurrentEnvironment(new BootStrap().init)
println "There are ${Post.count()} posts in the database"

println Post.list()

Post.where {
    user.loginId == "chuck_norris"
}.list()