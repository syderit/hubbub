import com.grailsinaction.User
import org.springframework.context.MessageSource

MessageSource messageSource = ctx.getBean("messageSource")
Locale locale = Locale.getDefault()

def user = new User(loginId: 'juan', password: '12345', homepage: 'not-a-url')
if (user.validate()) {
    println "everything fine"
} else {
    println "user has errors:"
    user.errors.fieldErrors.each {
       println "error in field: $it.field. Value '$it.rejectedValue' is invalid. Reason: $it.code. Full description: ${messageSource.getMessage(it, locale)}"
    }
}