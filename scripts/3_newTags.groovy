import com.grailsinaction.*

def userJuan = new User (loginId:"juan2", password:"secret")
def userPedro = new User (loginId:"peter2", password:"s3cr3t")

userJuan.save()
userPedro.save()

def tagJava = new Tag(name: "Java")
def tagSQL = new Tag(name: "SQL")
def tagMedia = new Tag(name: "media")
def tagTV = new Tag(name: "TV")
def tagDomotics = new Tag(name: "domotics")

userJuan.addToTags(tagJava)
userJuan.addToTags(tagSQL)

userPedro.addToTags(tagMedia)
userPedro.addToTags(tagTV)
userPedro.addToTags(tagDomotics)

def postJuanSQLJava = new Post(content: "this is an interesting SQL and Java post by Juan")
def postJuanJavaDomotics = new Post(content: "this is an interesting domotics post by Juan")
def postPedroTV = new Post(content: "this is an interesting TV post by Pedro")
def postPedroTVDomotics = new Post(content: "this is an interesting TV and domotics post by Pedro")

postJuanSQLJava.addToTags(tagSQL)
postJuanSQLJava.addToTags(tagJava)
postJuanJavaDomotics.addToTags(tagJava)
postJuanJavaDomotics.addToTags(tagDomotics)
postPedroTV.addToTags(tagTV)
postPedroTVDomotics.addToTags(tagTV)
postPedroTVDomotics.addToTags(tagDomotics)

userJuan.addToPosts(postJuanSQLJava)
userJuan.addToPosts(postJuanJavaDomotics)
userPedro.addToPosts(postPedroTV)
userPedro.addToPosts(postPedroTVDomotics)
