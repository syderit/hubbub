import com.grailsinaction.Post
import grails.util.Environment

Environment.executeForCurrentEnvironment(new BootStrap().init)
println "There are ${Post.count()} posts in the database"

def posts = Post.list()

for (p in posts) {
    println p.user.profile.fullName
}